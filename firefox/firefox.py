__author__ = 'crypt'

import os
import configparser
import sqlite3
import nltk
import math
from nltk.stem import *

def identify_profile():
    user = os.getenv("USER")
    home = os.getenv("HOME")
    main_profile_dir = None
    firefox_path = os.path.join(home, ".mozilla", "firefox")
    profile_ini = os.path.join(home, firefox_path, "profiles.ini")
    if os.path.isfile(profile_ini):
        profile_ini_config = configparser.ConfigParser()
        profile_ini_config.read(profile_ini)
        main_profile_dir_name = profile_ini_config['Profile0']['Path']
        main_profile_dir = os.path.join(firefox_path, main_profile_dir_name)
    else:
        print("User's profiles file")
    print("Checking user '{user}' profile at '{profile}'".format(user=user, profile=main_profile_dir))
    return main_profile_dir



def read_bookmark(profile_dir):
    bookmark_file = os.path.join(profile_dir, 'places.sqlite')
    if os.path.isfile(bookmark_file):
        conn = sqlite3.connect(bookmark_file)
        c = conn.cursor()
        c.execute("SELECT * from moz_bookmarks;")
        data = c.fetchall()
        return data
    else:
        return

def count_duplicate(data):
    print("Your profile contains".format(count=len(data)), end='')
    unique_set = set()
    for bookmark in data:
        unique_set.add(bookmark[5])

    percent = math.ceil(len(unique_set)/len(data)*10000)/100
    print(" #{count} bookmarks, with #{unique} ({percent}%) unique"
          .format(count=len(data), unique=len(unique_set), percent=percent), end='')


def stem_words(data):
    stemmer = SnowballStemmer("English")

    for bookmark in data:
        words = nltk.tokenize.word_tokenize(bookmark[5])
        #stemmer.stem()
        print(words)

def reader():
    profile_dir = identify_profile()
    if profile_dir:
        data = read_bookmark(profile_dir)
        count_duplicate(data)
        stem_words(data)
    else:
        print("Unable to identify a valid firefox profile")

if __name__ == '__main__':
    reader()