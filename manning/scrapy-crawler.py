__author__ = 'crypt'
"""

from scrapy import Spider, Item, Field


class Book(Item):
    title = Field()


class ManningSpider(Spider):
    name, start_urls = 'manningSpider', ['http://www.manning.com/catalog/']

    def parse(self, response):
        return [Book(title=e.extract()) for e in response.css("h2 a::text")]
"""