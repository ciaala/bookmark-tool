import sqlite3
import sys
import nltk

firefox_bookmark_filename = r"C:\Users\crypt\AppData\Roaming\Mozilla\Firefox\Profiles\zud6rm1k.default\places.sqlite"


def get_cursor(filename):
    db = sqlite3.connect(filename)
    c = db.cursor()
    return c


def match(data, rule):
    crule = rule.upper()
    i = 0
    found = 0
    items = len(data)
    subset_data = list()
    while i < items:
        if data[i][5] and crule in data[i][5].upper():
            value = "\n%04d %04d %s" % (found, i, data[i][5])
            subset_data.append(data[i])
            sys.stdout.write(value)
            found += 1
        i += 1
    sys.stdout.write("\n")
    return subset_data


def get_tables(db):
    db.execute(r"Select name from sqlite_master where type='table'")
    tables = db.fetchall()
    print(tables)
    for table in tables:
        data = get_data(db, table[0])
        print(table[0] + " #" + str(len(data)))


def get_data(db, table):
    db.execute(r"Select * from " + table)
    data = db.fetchall()
    return data


def main():
    db = get_cursor(firefox_bookmark_filename)
    data = get_data(db, r"moz_bookmarks")
    tokens = set()
    subset = match(data, "open")
    for sentence in subset:
        tokens = tokens.union(nltk.word_tokenize(sentence[5]))
#    tagged = nltk.pos_tag(tokens)
#    print(tagged[0:6])
    print(tokens)


if  __name__ == "__main__":
    main()

    db = get_cursor(firefox_bookmark_filename)
    get_tables(db);

